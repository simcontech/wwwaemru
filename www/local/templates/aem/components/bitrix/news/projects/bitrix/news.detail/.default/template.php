<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="project-details">
	<div class="item-properties">
	<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <div class="item-property property_<?=$pid?>">
                <span class="item-property-name"><?=$arProperty["NAME"]?>:</span>
                <span class="item-property-value <?=$arProperty["VALUE_XML_ID"]?>">
                    <?if($pid == 'services'):?>
                        <?foreach ($arProperty["DISPLAY_VALUE"] as $display_value):?>
                            <?
                            $display_value = preg_replace("/<?a(.*)>/", "<span$1>", $display_value);
                            $display_value = preg_replace("/<\/a>/", "</span>", $display_value);
                            $display_value = preg_replace("/href=\"[^\"]+\/(\w+)\/\"/", "class=\"services-icon ico_$1\"", $display_value);
                            ?>
                            <?=$display_value?>
                        <?endforeach;?>
                    <?elseif($pid == 'foto_video'):?>
                        <div class="fotorama"
                             data-nav="thumbs" 
                             data-navposition="bottom" 
                             data-navwidth="1000" 
                             data-arrows="false" 
                             data-width="100%" 
                             data-ratio="4/3"
                             data-thumbheight="115"
                             data-thumbmargin="30">
                        <?foreach ($arProperty["VALUE"] as $fileId):
                            $file = CFile::GetByID($fileId)->Fetch();
                            $filePath = CFile::GetPath($fileId);
                            if(substr_compare("image",$file["CONTENT_TYPE"],0,5,true) === 0 ):
                                $fileThumb = CFile::ResizeImageGet( 
                                        $fileId, 
                                        Array("width"=>165, "height"=>115),
                                        BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            ?>
                            <a href="<?=$filePath?>">
                                <img src="<?=$fileThumb["src"]?>" 
                                     width="<?=$fileThumb["width"]?>" 
                                     height="<?=$fileThumb["height"]?>"></a>
                            <?endif;?>
                        <?endforeach;?>
                        </div>
                    <?else:?>
                    <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                        <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
                    <?else:?>
                        <?=$arProperty["DISPLAY_VALUE"];?>
                    <?endif?>
                    <?endif?>
                </span>
            </div>
	<?endforeach;?>
        </div> 
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
                <h3 class="item-name"><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
                <div class="item-preview-text"><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></div>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
                <div class="item-detail-text"><?echo $arResult["DETAIL_TEXT"];?></div>
	<?endif?>
        <div class="item-fields">
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else if ($value != '')
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;?>
        </div>
          
	<?if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>