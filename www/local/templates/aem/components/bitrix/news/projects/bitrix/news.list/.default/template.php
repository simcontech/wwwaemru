<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="projects-list projects-tile">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="project-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                    <img
                            class="preview_picture"
                            border="0"
                            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                            width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                            height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            />
		<?endif?>
                    
                <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                    <a class="item-link" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                <?endif;?>
                
                <span class="item-link-content">       
                    
                    <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                        <span class="item-name"><?echo $arItem["NAME"]?></span>
                    <?endif;?>
                    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                        <span class="item-description"><?echo $arItem["PREVIEW_TEXT"];?></span>
                    <?endif;?>
                    <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                        <span class="item-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
                    <?endif?>
                    <?foreach($arItem["FIELDS"] as $code=>$value):?>
                            <span class="item-field field_<?=$code?>">
                                <span class="item-field-name"><?=GetMessage("IBLOCK_FIELD_".$code)?>:
                                <span class="item-field-value"><?=$value;?></span>
                            </span>
                    <?endforeach;?>
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                            <span class="item-property property_<?=$pid?>">
                                <span class="item-property-name"><?=$arProperty["NAME"]?>:</span>
                                <span class="item-property-value <?=$arProperty["VALUE_XML_ID"]?>">
                                    <?if($pid == 'services'):?>
                                        <?foreach ($arProperty["DISPLAY_VALUE"] as $display_value):?>
                                            <?
                                            $display_value = preg_replace("/<?a(.*)>/", "<span$1>", $display_value);
                                            $display_value = preg_replace("/<\/a>/", "</span>", $display_value);
                                            $display_value = preg_replace("/href=\"[^\"]+\/(\w+)\/\"/", "class=\"services-icon ico_$1\"", $display_value);
                                            ?>
                                            <?=$display_value?>
                                        <?endforeach;?>
                                    <?else:?>
                                    <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                        <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
                                    <?else:?>
                                        <?=$arProperty["DISPLAY_VALUE"];?>
                                    <?endif?>
                                    <?endif?>
                                </span>
                            </span>
                    <?endforeach;?>
                </span> 
                            
                <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                    </a>
                <?endif;?>
                    
	</div>
<?endforeach;?>

</div>
