/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $("<div class='fotorama_custom__arr fotorama_custom__arr--prev'>&nbsp;</div>").insertBefore(".fotorama__nav--thumbs");
    $("<div class='fotorama_custom__arr fotorama_custom__arr--next'>&nbsp;</div>").insertAfter(".fotorama__nav--thumbs");

    var $fotoramaDiv = $('.fotorama').fotorama();
    var fotorama = $fotoramaDiv.data('fotorama');
    $('.fotorama_custom__arr--prev').click(function () {
        fotorama.show('<');
    });
    $('.fotorama_custom__arr--next').click(function () {
        fotorama.show('>');
    });
    
    $('.property_status').appendTo('#pagetitle');
})