<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (empty($arResult))
    return;
?>
<ul id="sidebar-links">
<?
foreach ($arResult as $arItem):
    $arItem["ID"] = preg_replace("/\W/","",$arItem["LINK"]);
    ?>
        <li id="link_<?=$arItem["ID"]?>">
            <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
        </li>
    <?
endforeach;
?>
</ul>
