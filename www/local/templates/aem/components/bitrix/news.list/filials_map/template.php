<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="filials_map_wrapper">
    <div id="filials_map">
    </div>

    <script>
        window._aem_filialsMapDisplayed = false;
        window._aem_initMap = function () {
            var moscow = {lat: 55.911152, lng: 37.7488323};
            window.__aem_map = new google.maps.Map(document.getElementById('filials_map'), {
                zoom: 7,
                center: moscow,
                scrollwheel: false,
                disableDefaultUI: true,
                styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });
            if (typeof ($) != 'undefined' && $(document).scrollTop() < ($('#filials_map').offset().top - $(window).height())) {
                $(document).scroll(function () {
                    if (!window._aem_filialsMapDisplayed
                            && $(document).scrollTop() > ($('#filials_map').offset().top - $(window).height())) {
                        showMap();
                    }
                });
            } else {
                showMap();
            }
        }
        function showMap() {
            window._aem_filialsMapDisplayed = true;

            var marker_icon_main = '/local/templates/aem/images/marker_main.png';
            var marker_icon_filial = '/local/templates/aem/images/marker_filial.png';
            var bounds = new google.maps.LatLngBounds();
            var markers = [];
            /* markers.push(
             new google.maps.Marker({
             position: moscow,
             map: map
             })
             ); */

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $coordinates = explode(",", $arItem["DISPLAY_PROPERTIES"]["coordinates"]["VALUE"], 2);
    $lat = $coordinates[0];
    $lon = $coordinates[1];
    ?>
                markers.push(
                        new google.maps.Marker({
                            position: {lat: <?= $lat ?>, lng: <?= $lon ?>},
                            title: '<?= arItem["NAME"] ?>',
                            icon: markers.length > 0 ? marker_icon_filial : marker_icon_main,
                            animation: google.maps.Animation.DROP,
                            map: window.__aem_map 
                        })
                        );
                bounds.extend(new google.maps.LatLng(<?= $lat ?>, <?= $lon ?>));
<? endforeach; ?>
//            google.maps.event.addListenerOnce(window.__aem_map, 'idle', function () {
                window.setTimeout(function () {
                    window.__aem_map.fitBounds(bounds);       // auto-zoom
                    window.__aem_map.panToBounds(bounds);     // auto-center
                }, 1500);
//            });
        }
    </script>

</div>
