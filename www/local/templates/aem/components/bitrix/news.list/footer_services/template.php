<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? 
$i = 0;
$j = count($arResult["ITEMS"]) / 2 ;?>
<ul class="footer-links footer-services-list">
<? foreach ($arResult["ITEMS"] as $arItem): ?>
        <li class="services-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
        </li>
        <? $i++; ?>
        <?if($i == $j):?>
</ul>
<ul class="footer-links footer-services-list">
        <?endif;?>
<? endforeach; ?>
</ul>
