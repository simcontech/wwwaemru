<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):
    $previewPicture = CFile::ResizeImageGet( 
                                        $arItem["PREVIEW_PICTURE"]["ID"], 
                                        Array("width"=>600, "height"=>600),
                                        BX_RESIZE_IMAGE_PROPORTIONAL);
    endif;
    ?>

    <div class="services-item" data-preview-picture="<?=$previewPicture["src"]?>">
        <span class="services-icon ico_<? echo $arItem["CODE"] ?>" >&nbsp;</span>
        <a class="service-link" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
            <? echo $arItem["NAME"] ?>
        </a>
    </div>
<? endforeach; ?>
