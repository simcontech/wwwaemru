<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();?>
<?
IncludeTemplateLangFile(__FILE__);

global $currentDir;
global $isHomePage;
global $isSidebar;
global $isShowDateChange;

$currentSite = CSite::GetByID(SITE_ID);
$currentSite = $currentSite->Fetch();

$currentDir = $APPLICATION->GetCurDir();
$arCurrentDir = explode("/", $currentDir);

$isHomePage = ( $_SERVER["REQUEST_URI"] == '/' || $currentDir == '/');
$isSidebar = !$isHomePage
        &&(file_exists($_SERVER['DOCUMENT_ROOT']. SITE_DIR . $currentDir . '.menu1.menu.php') 
        || file_exists($_SERVER['DOCUMENT_ROOT']. SITE_DIR . $currentDir . '.sidebar.php'));
?> 
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1260, initial-scale=minimum-scale">
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/fotorama-4.6.4/fotorama.js"></script> 
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
        <?$APPLICATION->ShowHead();?>
        <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico" type="image/x-icon" />
        <link href="<?= SITE_TEMPLATE_PATH ?>/template_sitepages.css" type="text/css" rel="stylesheet" />
        <link href="<?= SITE_TEMPLATE_PATH ?>/js/fotorama-4.6.4/fotorama.css" rel="stylesheet">
        <title>АЭМ | <?$APPLICATION->ShowTitle()?></title>
        
    </head>
    <body class="<?= $isHomePage ? "homepage" : "innerpage " . str_replace('/', ' ', $currentDir) ?>">
        
        <div id="page-wrapper">
            
            <div id="panel"><?$APPLICATION->ShowPanel();?></div>

            <div id="header-wrapper" class="page-section-wrapper">
                
                <div id="header" class="page-section">
                    <div id="logo">
                        <a href="<?= SITE_DIR ?>" title="<?= GetMessage('CFT_MAIN') ?>">
                            &nbsp;
                        </a>
                    </div>
                    <div id="top-menu">
                        <div id="top-menu-inner">
                            <?$APPLICATION->IncludeComponent("bitrix:menu", "header", array(
                            "ROOT_MENU_TYPE" => "menu0",
                            "MAX_LEVEL" => "2",
                            "CHILD_MENU_TYPE" => "menu1",
                            "USE_EXT" => "Y",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                            ),
                            false,
                            array(
                            "ACTIVE_COMPONENT" => "Y"
                            )
                            );?>
                        </div>
                    </div>
                    <div id="lang-icons">
                        <a href="<?= SITE_DIR ?>" class="ru icon selected" title="<?= GetMessage('CFT_MAIN') ?>">RU</a>
                        <a href="<?= SITE_DIR ?>search/" class="en icon" title="<?= GetMessage('CFT_SEARCH') ?>">EN</a>
                    </div>
                    <div id="phone">
                        <a href="<?=  SITE_DIR ?>" title="<?= GetMessage('CFT_MAIN') ?>"><?
                            $APPLICATION->IncludeFile(
                            SITE_DIR."local/include/phone.php",
                            Array(),
                            Array("MODE"=>"html")
                            );
                            ?></a>
                    </div>
                </div>
                
            </div>
            
            <?if(!$isHomePage):?>
            <div id="pagetitle-wrapper" class="page-section-wrapper">
                <div id="pagetitle-background">&nbsp;</div>
                <h1 id="pagetitle" class="page-section">
                    <span><? $APPLICATION->ShowTitle(false);?></span>
                </h1>
            </div>
            <?endif;?>
            
            <div id="content-wrapper-outer" class="page-section-wrapper">
                
                <div id="content-wrapper-inner" class="page-section-wrapper">
                   
                    <div id="content" class="page-section <?= $isSidebar ? 'with-sidebar' : '' ?>">

                        <?if($isSidebar):?>
                        <div id="sidebar">
                            <div id="sidebar-menu">
                            <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:menu", 
                                    "sidebar", 
                                    array(
                                        "ROOT_MENU_TYPE" => "menu1",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_TIME" => "36000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(
                                        ),
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "ALLOW_MULTI_SELECT" => "N"
                                        ),
                                        false
                                );
                                ?>
                                </div>
                            <div id="sidebar-text">
                            <?
                                $APPLICATION->IncludeFile(
                                SITE_DIR . $currentDir . '.sidebar.php',
                                Array(),
                                Array("MODE"=>"html")
                                );
                            ?>
                            </div>
                        </div>
                        <?endif;?>

                        <div id="workarea">
                            