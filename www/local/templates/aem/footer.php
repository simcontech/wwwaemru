<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
                        </div><!-- END: workarea -->

                    </div><!-- END: content -->

                </div>
                <!-- END: content-wrapper-inner -->
                
            </div>
            <!-- END: content-wrapper-outer -->

            <div id="footer-wrapper" class="page-section-wrapper">

                <div id="footer-menu" class="page-section">	
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu", 
                        "footer", 
                        array(
                            "ROOT_MENU_TYPE" => "menu0",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "menu1",
                            "USE_EXT" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                            ),
                            false
                    );
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "footer_services",
                        Array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("", ""),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "products",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "20",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array("", ""),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "ASC"
                    )
            );?>
                </div>

                <div id="footer-text" class="page-section">
                    <div id="footer-company">
                        <?
                        $APPLICATION->IncludeFile(
                                SITE_DIR."local/include/footer_company.php",
                                Array(),
                                Array("MODE"=>"text")
                        );
                        ?>
                    </div>
                    <div id="footer-address">
                        <?
                        $APPLICATION->IncludeFile(
                                SITE_DIR."local/include/footer_address.php",
                                Array(),
                                Array("MODE"=>"text")
                        );
                        ?>
                    </div>
                    <div id="footer-phone">
                        <?
                        $APPLICATION->IncludeFile(
                                SITE_DIR."local/include/phone.php",
                                Array(),
                                Array("MODE"=>"text")
                        );
                        ?>
                    </div>
                </div>

            </div>
        
        </div>
        <!-- END: page-wrapper -->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn8zxUEBSKB3vvOHQXXoVb0_vfKrvnANs&callback=initMap&language=ru&region=RU">
        </script>
        <script>
            function initMap() {
                if(typeof(window._aem_initMap) == 'function'){
                    window._aem_initMap();
                }
            }
        </script>
    </body>
</html>