$(document).ready(function () {

    window._aem_dialogViewImage = function (imageUrl, imageDescription, downloadUrl) {
        if ($('#dialog-view-image').length === 0) {
            $('<div id="dialog-view-image"><a id="dialog-view-image-close" href="#">X</a><div id="dialog-view-image-content"></div></div>').appendTo('body');
            $('#dialog-view-image-close').click(function () {
                var $dialog = $('#dialog-view-image');
                $dialog.find('#dialog-view-image-content').html('');
                $dialog.hide();
                location.hash = '_';
                return false;
            });
        }
        var $dialog = $('#dialog-view-image');
        $dialog.hide();
        $dialog.find('#dialog-view-image-content').html('<img src="' + imageUrl + '"/>'
                + '<div class="image-description">' + imageDescription + '</div>'
                + (typeof (downloadUrl) !== 'undefined' && downloadUrl !== null ? '<a class="download-link" href="' + downloadUrl + '">Скачать</div>' : ''));
        $dialog.show();
    };

    window._aem_servicesCircleSectorClick = function (index) {
        location.href = $('#services_icons .services-item:nth-child(' + (index + 1) + ') a')[0].href;
    };
    window._aem_servicesCircleSectorHover = function (index) {
        $('#services_circle_sectors .sector_item,'
                + ' #services_circle_sectors_images img,'
                + ' #services_icons .services-item').removeClass('hover');
        $('#services_circle_sectors .sector_item:nth-child(' + (index + 1) + ')').addClass('hover');
        $('#services_circle_sectors_images img:nth-child(' + (index + 1) + ')').addClass('hover');
        $('#services_icons .services-item:nth-child(' + (index + 1) + ')').addClass('hover');
    };
    window._aem_servicesCircleSectorUnhover = function (index) {
        $('#services_circle_sectors .sector_item:nth-child(' + (index + 1) + ')').removeClass('hover');
        $('#services_circle_sectors_images img:nth-child(' + (index + 1) + ')').removeClass('hover');
        $('#services_icons .services-item:nth-child(' + (index + 1) + ')').removeClass('hover');
    };

    window._aem_servicesCircleSectorsPlayStart = function () {
        $('#services_icons .services-item').each(function (index) {
            if (index < 6) {
                window.setTimeout(function () {
                    $('#services_circle_sectors .sector_item:nth-child(' + (index + 1) + ')').addClass('start');
                    $('#services_circle_sectors .sector_item:nth-child(' + (index < 5 ? index + 6 : index - 4) + ')').addClass('start');
                }, 300 * (index + 1));
                window.setTimeout(function () {
                    $('#services_circle_sectors .sector_item:nth-child(' + (index + 1) + ')').removeClass('start');
                    $('#services_circle_sectors .sector_item:nth-child(' + (index < 5 ? index + 6 : index - 4) + ')').removeClass('start');
                }, 300 + 330 * (index + 1));
            }
        })
    };
    window._aem_projectsListAnimate = function () {
        $('.projects-list #tile-column1 .project-item:not(.animate-ready)').each(function (index) {
            var $item = $(this);
            if ($item.offset().top - ($(window).height() - 150) < $(document).scrollTop()) {
                $item.addClass('animate-ready');
                window.setTimeout(function () {
                    $item.addClass('animate')
                }, 500 * index);
            }
        });
        $('.projects-list #tile-column2 .project-item:not(.animate-ready)').each(function (index) {
            var $item = $(this);
            if ($item.offset().top - ($(window).height() - 150) < $(document).scrollTop()) {
                $item.addClass('animate-ready');
                window.setTimeout(function () {
                    $item.addClass('animate')
                }, 250 + 500 * index);
            }
        });
        $('.projects-list > .project-item:not(.animate-ready)').each(function (index) {
            var $item = $(this);
            if ($item.offset().top - ($(window).height() - 150) < $(document).scrollTop()) {
                $item.addClass('animate-ready');
                window.setTimeout(function () {
                    $item.addClass('animate')
                }, 250 * index);
            }
        });
    };

    /* min-height */
    $('#workarea').css(
            'min-height',
            ($('html,body').height()
                    - $('#header-wrapper').height()
                    - $('#pagetitle-wrapper').height()
                    - $('#footer-wrapper').height() - 100) + 'px');

    /* Home page */1
    if ($('body').hasClass('homepage')) {
        window.setTimeout(function () {
            $('#widget_services').addClass('circles1');
        }, 10);
        window.setTimeout(function () {
            $('#widget_services').addClass('loading');
        }, 500);
        window.setTimeout(function () {
            $('#widget_services').addClass('loaded');
        }, 1500);
        window.setTimeout(function () {
            $('#widget_services').addClass('circles2');
            window.setInterval(function () {
                $('#services_circles_pulsar').addClass('pulse');
                window.setTimeout(function () {
                    $('#services_circles_pulsar').removeClass('pulse')
                }, 3000);
            }, 4000);
        }, 4100);
        window.setTimeout(function () {
            $('#widget_services').addClass('circles3');
            window._aem_servicesCircleSectorsPlayStart();
        }, 4500);
        window.setTimeout(function () {
            $('#widget_services .services-item').each(function (index) {
                var $serviceItem = $(this);
                $serviceItem
                        .mousemove(function () {
                            window._aem_servicesCircleSectorHover(index)
                        })
                        .mouseout(function () {
                            window._aem_servicesCircleSectorUnhover(index)
                        });
                $('#services_circle_sectors_images')
                        .append('<img src="' + $serviceItem.attr('data-preview-picture') + '" />');

            });
            $('#services_circle_sectors_hover .sector_item').each(function (index) {
                var $sectorItem = $(this);
                $sectorItem
                        .click(function () {
                            window._aem_servicesCircleSectorClick(index)
                        })
                        .mousemove(function () {
                            window._aem_servicesCircleSectorHover(index)
                        })
                        .mouseout(function () {
                            window._aem_servicesCircleSectorUnhover(index)
                        });
            });
        }, 7000);

    }

    /* Page Company */
    if (location.pathname == '/company/') {
        window._aem_hashchangeHandler = function () {
            if (location.hash !== '' && location.hash !== '#' && location.hash !== '#_') {

                /* company info-block */
                if (location.hash.match(/^#\w+$/i) && $('#company_' + location.hash.substr(1)).length > 0) {
                    $('html, body').animate({
                        scrollTop: $('#company_' + location.hash.substr(1)).offset().top
                    }, 500);
                }

                /* licenses view */
                else if (location.hash.substr(1, 9) === 'licenses/') {
                    $('#company_licenses .info-item a').each(function () {
                        if ($(this).attr('href').indexOf(location.hash) >= 0) {
                            var $l = $($(this).parents('.info-item')[0]);
                            var $a = $l.find('.field_DETAIL_PICTURE a');
                            window._aem_dialogViewImage($a.attr('href'), $a.attr('title'), $a.attr('href') + '?download');
                            return false;
                        }
                    });
                }
            } else {
                return false;
            }
        };
        if (location.hash !== '' && location.hash !== '#') {
            window._aem_hashchangeHandler();
        }
        $(window).on('hashchange', window._aem_hashchangeHandler);
    }

    /* Projects tile */
    if ($('.projects-list.projects-tile').length > 0) {
        $('.projects-list.projects-tile').append('<div id="tile-column1" class="tile-column"></div><div id="tile-column2" class="tile-column"><span></span></div>');
        $('.projects-list > .project-item:nth-child(odd)').appendTo('.projects-list.projects-tile #tile-column1');
        $('.projects-list > .project-item').appendTo('.projects-list.projects-tile #tile-column2');
        window._aem_projectsListAnimate();
        $(document).scroll(function () {
            window._aem_projectsListAnimate()
        });
    } else if ($('.projects-list').length > 0) {
        window._aem_projectsListAnimate();
        $(document).scroll(function () {
            window._aem_projectsListAnimate()
        });
    }

});